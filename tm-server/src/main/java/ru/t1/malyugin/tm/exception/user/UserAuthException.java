package ru.t1.malyugin.tm.exception.user;

public final class UserAuthException extends AbstractUserException {

    public UserAuthException() {
        super("Error! Wrong credentials...");
    }

}