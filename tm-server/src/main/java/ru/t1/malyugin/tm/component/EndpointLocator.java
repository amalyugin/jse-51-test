package ru.t1.malyugin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.*;
import ru.t1.malyugin.tm.api.service.IEndpointLocator;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.endpoint.*;

import javax.xml.ws.Endpoint;

public final class EndpointLocator implements IEndpointLocator {

    @NotNull
    private final IServiceLocator serviceLocator;

    private ISystemEndpoint systemEndpoint;

    private IProjectEndpoint projectEndpoint;

    private ITaskEndpoint taskEndpoint;

    private IUserEndpoint userEndpoint;

    private IAuthEndpoint authEndpoint;

    private ISchemeEndpoint schemeEndpoint;

    public EndpointLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        initEndpointList();
        registryEndpointList();
    }

    private void initEndpointList() {
        systemEndpoint = new SystemEndpoint(serviceLocator);
        projectEndpoint = new ProjectEndpoint(serviceLocator);
        taskEndpoint = new TaskEndpoint(serviceLocator);
        userEndpoint = new UserEndpoint(serviceLocator);
        authEndpoint = new AuthEndpoint(serviceLocator);
        schemeEndpoint = new SchemeEndpoint(serviceLocator);
    }

    private void registryEndpointList() {
        registryEndpoint(systemEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(authEndpoint);
        registryEndpoint(schemeEndpoint);
    }

    private void registryEndpoint(@NotNull final IEndpoint endpoint) {
        @NotNull final String host = serviceLocator.getPropertyService().getServerHost();
        @NotNull final String port = serviceLocator.getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}