package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.dto.IDTORepository;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.dto.IDTOService;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractDTOService<M extends AbstractDTOModel>
        implements IDTOService<M> {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractDTOService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return serviceLocator.getConnectionService().getEntityManager();
    }

    @NotNull
    protected abstract IDTORepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findOneById(id.trim());
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(id);
        if (model == null) return;
        remove(model);
    }

}