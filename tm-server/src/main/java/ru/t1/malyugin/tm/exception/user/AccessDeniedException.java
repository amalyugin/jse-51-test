package ru.t1.malyugin.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You are not logged in...");
    }

}