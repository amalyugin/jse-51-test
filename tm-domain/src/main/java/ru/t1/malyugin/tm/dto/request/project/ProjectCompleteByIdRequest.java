package ru.t1.malyugin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectCompleteByIdRequest(
            @Nullable final String token,
            @Nullable final String id
    ) {
        super(token);
        this.id = id;
    }

}