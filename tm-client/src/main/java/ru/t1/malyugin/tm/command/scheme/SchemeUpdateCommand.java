package ru.t1.malyugin.tm.command.scheme;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.scheme.SchemeUpdateRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class SchemeUpdateCommand extends AbstractSchemeCommand {

    @NotNull
    private static final String NAME = "scheme-update";

    @NotNull
    private static final String DESCRIPTION = "Update scheme";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }


    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-su";
    }

    @Override
    public void execute() {
        System.out.println("[SCHEME UPDATE]");
        System.out.println("ENTER INIT TOKEN: ");
        @NotNull final String initToken = TerminalUtil.nextLine();
        @NotNull final SchemeUpdateRequest request = new SchemeUpdateRequest(initToken);
        getSchemeEndpoint().updateScheme(request);
    }

}
